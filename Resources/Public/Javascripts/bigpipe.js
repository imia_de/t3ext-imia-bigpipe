function bigPipeInsert(intScriptId) {
    var replaceElement = document.getElementById(intScriptId);
    var insertElement = document.getElementById(intScriptId + '_ins');

    if (replaceElement && insertElement) {
        var insertFragment = document.createDocumentFragment();
        while (insertElement.firstChild) {
            insertFragment.appendChild(insertElement.firstChild);
        }

        replaceElement.parentNode.replaceChild(insertFragment, replaceElement);
        var script = insertElement.nextSibling;
        insertElement.parentNode.removeChild(insertElement);
        script.parentNode.removeChild(script);
    }
}

function triggerEvent(eventName, element) {
    if (typeof jQuery !== "undefined") {
        jQuery(element).trigger(eventName);
    } else if (typeof CustomEvent != 'undefined') {
        element.dispatchEvent(
            new CustomEvent(eventName)
        );
    } else {
        var event;
        if (document.createEvent) {
            event = document.createEvent("HTMLEvents");
            event.initEvent(eventName, true, true);
        } else {
            event = document.createEventObject();
            event.eventType = eventName;
        }
        event.eventName = eventName;

        if (element.createEvent) {
            element.dispatchEvent(event);
        } else {
            element.fireEvent('on' + event.eventType, event);
        }
    }
}

triggerEvent('bigpipe', document);