<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2013 IMIA net based solutions (d.frerich@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaBigpipe\Xclass\Frontend\Controller;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_bigpipe
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptFrontendController extends \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
{
    /**
     * @var array
     */
    protected $INTScripts;

    /**
     * @var boolean
     */
    protected $bigpipe = true;

    /**
     * @var boolean
     */
    protected $renderINT = false;

    public function INTincScript()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->bigpipe = false;
        } else {
            foreach ($_GET as $param) {
                if (is_array($param) && array_key_exists('format', $param) && strtolower($param['format']) != 'html') {
                    $this->bigpipe = false;
                }
            }
        }

        parent::INTincScript();

        if ($this->bigpipe) {
            $this->prepareINTForBigPipe();
        }
    }

    public function hook_eofe()
    {
        parent::hook_eofe();

        if ($this->bigpipe && $GLOBALS['sendTSFEContent']) {
            $GLOBALS['TSFE']->config['config']['admPanel'] = false; // force hidden admin panel

            $bodyClose = strripos($GLOBALS['TSFE']->content, '</body');
            if ($bodyClose !== false) {
                $this->flushPrint(substr($GLOBALS['TSFE']->content, 0, $bodyClose));
                $GLOBALS['TSFE']->content = substr($GLOBALS['TSFE']->content, $bodyClose);
            }

            $this->flushPrint('<style type="text/css">.bigpipe-insert{ height:0.01px; overflow: hidden; position: relative}</style>');
            $this->flushPrint('<script src="' . ExtensionManagementUtility::extRelPath('imia_bigpipe') .
                'Resources/Public/Javascripts/bigpipe.js"></script>');

            $this->renderINT = true;
            $tmpContent = $GLOBALS['TSFE']->content;
            foreach ($this->INTScripts as $intScriptId) {
                $GLOBALS['TSFE']->content =
                    '<div id="' . $intScriptId . '_ins" class="bigpipe-insert"><!--' . $intScriptId. '--></div>' .
                    '<script>bigPipeInsert("' . $intScriptId . '")</script>';

                $this->recursivelyReplaceIntPlaceholdersInContent();
                $this->flushPrint($GLOBALS['TSFE']->content);
            }

            $GLOBALS['TSFE']->content = $tmpContent;
        }
    }

    
    protected function recursivelyReplaceIntPlaceholdersInContent() 
    {
        if (!$this->bigpipe || $this->renderINT) {
            parent::recursivelyReplaceIntPlaceholdersInContent();
        }
    }

    /**
     * @param $content
     */
    protected function flushPrint($content)
    {
        flush();
        ob_end_flush();
        @ini_set('output_buffering', '0');
        ob_implicit_flush(true);

        echo $content;
    }

    protected function prepareINTForBigPipe()
    {
        $regex = '/<!\-\-(INT_SCRIPT\.[^-]+)\-\->/ism';
        preg_match_all($regex, $this->content, $matches);

        $this->INTScripts = $matches[1];
        $this->content = preg_replace($regex, '<div id="$1" class="bigpipe-loading"></div>', $this->content);
    }
}